#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
	CREATE USER $BACKEND_DB_USER WITH PASSWORD '$BACKEND_DB_PASSWORD';
	CREATE DATABASE $BACKEND_DB_NAME OWNER $BACKEND_DB_USER;
	GRANT ALL PRIVILEGES ON DATABASE $BACKEND_DB_NAME TO $BACKEND_DB_USER;

\c $BACKEND_DB_NAME

CREATE TABLE public.customers (
    id integer NOT NULL,
    name character varying(80) NOT NULL,
    surname character varying(80) NOT NULL,
    birthday date NOT NULL,
    email character varying(256) NOT NULL
);

ALTER TABLE public.customers OWNER TO $BACKEND_DB_USER;

CREATE SEQUENCE public.customers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.customers_id_seq OWNER TO $BACKEND_DB_USER;
ALTER SEQUENCE public.customers_id_seq OWNED BY public.customers.id;

CREATE TABLE public.movies (
    id integer NOT NULL,
    name character varying NOT NULL,
    year integer NOT NULL,
    duration bigint NOT NULL
);


ALTER TABLE public.movies OWNER TO $BACKEND_DB_USER;

CREATE SEQUENCE public.movies_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.movies_id_seq OWNER TO $BACKEND_DB_USER;
ALTER SEQUENCE public.movies_id_seq OWNED BY public.movies.id;


CREATE TABLE public.sessions (
    id bigint NOT NULL,
    start_time timestamp without time zone NOT NULL,
    customer_id integer NOT NULL,
    movie_id integer NOT NULL
);

ALTER TABLE public.sessions OWNER TO $BACKEND_DB_USER;

CREATE SEQUENCE public.sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.sessions_id_seq OWNER TO $BACKEND_DB_USER;
ALTER SEQUENCE public.sessions_id_seq OWNED BY public.sessions.id;

ALTER TABLE ONLY public.customers ALTER COLUMN id SET DEFAULT nextval('public.customers_id_seq'::regclass);
ALTER TABLE ONLY public.movies ALTER COLUMN id SET DEFAULT nextval('public.movies_id_seq'::regclass);
ALTER TABLE ONLY public.sessions ALTER COLUMN id SET DEFAULT nextval('public.sessions_id_seq'::regclass);

ALTER TABLE ONLY public.customers ADD CONSTRAINT customers_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.movies ADD CONSTRAINT movies_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.sessions ADD CONSTRAINT sessions_pkey PRIMARY KEY (id);

CREATE INDEX ON public.sessions(customer_id);
CREATE INDEX ON public.sessions(movie_id);
EOSQL