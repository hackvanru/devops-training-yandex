# Final task for the DevOps training by Yandex

* [Сourse description](https://yandex.ru/yaintern/training/devops-training)
* [Task](https://disk.yandex.ru/i/lHRYD7kWCnQTxg)

## How to deploy
1. Install requirements for Ansible
``` bash
ansible-galaxy collection install cloud.terraform
```

2. Generate key for machine
``` bash
ssh-keygen -t rsa -f bingo
```

3. Initializes a working directory containing configuration files and installs plugins for required providers.
``` bash
terraform init
```

4. Create private container registry
``` bash
terraform plan -target yandex_container_registry.registry1
terraform apply -target yandex_container_registry.registry1
```
Write down your container id from the command output and make variable

``` bash
export CONTAINER_ID=your_id
```

5. Build contaiter
``` bash
cd bingo
docker build --no-cache -t backend .
```

6.  Push contaiter to private container registry
``` bash
cat tf_key.json | docker login --username json_key --password-stdin cr.yandex
docker tag backend cr.yandex/${CONTAINER_ID}/backend:1
docker push cr.yandex/${CONTAINER_ID}/backend:1
```

7. Build infrastructe
``` bash
terraform plan
terraform apply
```

8. Deploy configuration for Bingo
``` bash
ansible-playbook -i inventory.yml playbook.yml
```

## Monitoring

http://your_ip:3000/

Login: admin\
Password: nHElI7mH